﻿
using Crafting;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(RecipeIngredient))]
public class RecipeIngredientDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        
        var width = position.width;
        position.width = width / 2;
        property.FindPropertyRelative("ingredient").objectReferenceValue = EditorGUI.ObjectField(position, property.FindPropertyRelative("ingredient").objectReferenceValue,
            typeof(Ingredient), false);
        position.x += width / 2; 
        property.FindPropertyRelative("amount").intValue = EditorGUI.IntSlider(position, property.FindPropertyRelative("amount").intValue, 0, 99);
        //property.FindPropertyRelative("amount").intValue = Mathf.Max(0,EditorGUI.IntField(position, property.FindPropertyRelative("amount").intValue));
        
        EditorGUI.EndProperty();
    }
}