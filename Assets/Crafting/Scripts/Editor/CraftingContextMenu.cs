﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.IO;
using Crafting;
using System;

public class CraftingContextMenu
{
    [MenuItem("Assets/Crafting/Create Ingredients from Sprites", true)]
    static bool CreateIngredientsValidate()
    {
        for (int i = 0; i < Selection.assetGUIDs.Length; ++i)
        {
            var selected = Selection.assetGUIDs[i];
            var assetPath = AssetDatabase.GUIDToAssetPath(selected);
            
            var icon = AssetDatabase.LoadAssetAtPath<Sprite>(assetPath);
            if (icon == null) return false;
        }

        return true;
    }
    
    [MenuItem("Assets/Crafting/Create Ingredients from Sprites", false)]
    static void CreateIngredients()
    {
        var folder = string.Empty;
        List<UnityEngine.Object> newAssets = new List<UnityEngine.Object>();
        
        for (int i = 0 ; i < Selection.assetGUIDs.Length ; ++i)
        {
            var selected = Selection.assetGUIDs[i];
            var assetPath = AssetDatabase.GUIDToAssetPath(selected);
            if (string.IsNullOrEmpty(assetPath))
            {
                Debug.LogWarning($"Asset path for guid {selected} is null");
                continue;
            }
            var ingredient = CreateIngredient(selected);
            var assetName = Path.GetFileName(assetPath);
            if (string.IsNullOrEmpty(assetName)) throw new Exception("Asset Name cannot be null");
                
            var progress = i / (float) Selection.assetGUIDs.Length;
            EditorUtility.DisplayProgressBar("Crafting", $"Creating Ingredient Assets {assetName}", progress);
            
            folder = assetPath.Substring(0, assetPath.Length - assetName.Length);
            var ingredientPath = folder + ingredient.DisplayName + ".asset";
            AssetDatabase.CreateAsset(ingredient, ingredientPath);
            newAssets.Add(AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(ingredientPath));
        }
        
        AssetDatabase.ImportAsset(folder);
        EditorUtility.ClearProgressBar();
        Selection.objects = newAssets.ToArray();
    }

    [MenuItem("Assets/Crafting/Create Recipes from Ingredients", true)]
    static bool CreateRecipesValidate()
    {
        for (int i = 0; i < Selection.assetGUIDs.Length; ++i)
        {
            var selected = Selection.assetGUIDs[i];
            var assetPath = AssetDatabase.GUIDToAssetPath(selected);
            
            var ingredient = AssetDatabase.LoadAssetAtPath<Ingredient>(assetPath);
            if (ingredient == null) return false;
        }

        return true;
    }
    
    [MenuItem("Assets/Crafting/Create Recipes from Ingredients", false)]
    static void CreateRecipes()
    {
        var folder = string.Empty;
        List<UnityEngine.Object> newAssets = new List<UnityEngine.Object>();
        
        for (int i = 0; i < Selection.assetGUIDs.Length; ++i)
        {
            var selected = Selection.assetGUIDs[i];
            var assetPath = AssetDatabase.GUIDToAssetPath(selected);
            if (string.IsNullOrEmpty(assetPath))
            {
                Debug.LogWarning($"Asset path for guid {selected} is null");
                continue;
            }
            var assetName = Path.GetFileName(assetPath);
            if (string.IsNullOrEmpty(assetName)) throw new Exception("Asset Name cannot be null");
            
            var progress = i / (float) Selection.assetGUIDs.Length;
            EditorUtility.DisplayProgressBar("Crafting", $"Creating Recipes from ingredient {assetName}", progress);

            var recipe = CreateRecipe(selected);
            var recipeName = "Recipe for " + Path.GetFileNameWithoutExtension(assetPath);
            folder = assetPath.Substring(0, assetPath.Length - assetName.Length);
            var recipePath = folder + recipeName + ".asset";
            AssetDatabase.CreateAsset(recipe, recipePath);
            newAssets.Add(AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(recipePath));
        }
        
        AssetDatabase.ImportAsset(folder);
        EditorUtility.ClearProgressBar();
        Selection.objects = newAssets.ToArray();
    }

    [MenuItem("Assets/Crafting/Create Book from Recipes", true)]
    static bool CreateBookValidate()
    {
        for (int i = 0; i < Selection.assetGUIDs.Length; ++i)
        {
            var selected = Selection.assetGUIDs[i];
            var assetPath = AssetDatabase.GUIDToAssetPath(selected);
            
            var icon = AssetDatabase.LoadAssetAtPath<Recipe>(assetPath);
            if (icon == null) return false;
        }

        return true;
    }

    [MenuItem("Assets/Crafting/Create Book from Recipes", false)]
    static void CreateBook()
    {
        List<Recipe> recipes = new List<Recipe>();
        
        for (int i = 0; i < Selection.assetGUIDs.Length; ++i)
        {
            var selected = Selection.assetGUIDs[i];
            var assetPath = AssetDatabase.GUIDToAssetPath(selected);
            if (string.IsNullOrEmpty(assetPath))
            {
                // Debug.LogWarning($"Asset path for guid {selected} is null");
                continue;
            }
            
            var progress = i / (float) Selection.assetGUIDs.Length;
            EditorUtility.DisplayProgressBar("Crafting", $"Creating Book", progress);

            recipes.Add(AssetDatabase.LoadAssetAtPath<Recipe>(assetPath));
        }

        var book = CreateBook(recipes.ToArray());
        var randomAssetPath = AssetDatabase.GUIDToAssetPath(Selection.assetGUIDs[0]);
        var folder = randomAssetPath.Substring(0, randomAssetPath.Length - Path.GetFileNameWithoutExtension(randomAssetPath).Length);
        var bookPath = folder + "recipe book.asset";
        AssetDatabase.CreateAsset(book, bookPath);
        if (AssetDatabase.IsValidFolder(folder)) AssetDatabase.ImportAsset(folder);
        EditorUtility.ClearProgressBar();
        Selection.objects = new []{AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(bookPath)};
    }

    private static RecipeBook CreateBook(Recipe[] recipes)
    {
        return RecipeBook.NewRecipeBook(recipes);
    }

    private static Recipe CreateRecipe(string selected)
    {
        var ingredientAPath = AssetDatabase.GUIDToAssetPath(selected);
        var ingredientA = AssetDatabase.LoadAssetAtPath<Ingredient>(ingredientAPath);
        if (ingredientA == null)
        {
            throw new Exception($"Could not load ingredients {ingredientAPath}");
        }

        var recipe = Recipe.NewRecipe(new RecipeIngredient[0], ingredientA);
        return recipe;
    }

    private static Ingredient CreateIngredient(string selected)
    {
        var assetPath = AssetDatabase.GUIDToAssetPath(selected);
        var name = Path.GetFileNameWithoutExtension(assetPath);
        var icon = AssetDatabase.LoadAssetAtPath<Sprite>(assetPath);
        if (icon == null)
        {
            Debug.LogWarning($"Icon at path {assetPath} is null. It should be the SPRITE type.");
        }
        
        var ingredient = Ingredient.NewIngredient(name, icon);
        return ingredient;
    }
}
