﻿using UnityEditor;
using UnityEngine;
using Crafting;

[CustomEditor(typeof(Ingredient))]
public class IngredientEditor : Editor
{
    public override bool HasPreviewGUI()
    {
        var ingredient = target as Ingredient;
        return ingredient != null && ingredient.Icon != null && ingredient.Icon.texture != null;
    }
    
    public override void OnPreviewGUI(Rect r, GUIStyle background)
    {
        var ingredient = target as Ingredient;
        if (ingredient == null) return;
        var sprite = ingredient.Icon;
        GUI.DrawTexture(r, sprite.texture, ScaleMode.ScaleToFit, true);
    }
    
    public override Texture2D RenderStaticPreview(string assetPath, Object[] subAssets, int width, int height)
    {
        var ingredient = target as Ingredient;
        if (ingredient == null || ingredient.Icon == null || ingredient.Icon.texture == null) return null;
        
        // if not decompressed, a error is thrown
        // Unsupported texture format - Texture2D::EncodeTo functions do not support compressed texture formats.
        Texture2D source = ingredient.Icon.texture;
        return DeCompress(source);
    }

    private static Texture2D DeCompress(Texture2D source)
    {
        RenderTexture renderTex = RenderTexture.GetTemporary(
            source.width,
            source.height,
            0,
            RenderTextureFormat.Default,
            RenderTextureReadWrite.Linear);
        Graphics.Blit(source, renderTex);
        
        Texture2D dest = new Texture2D(renderTex.width, renderTex.height, TextureFormat.RGBA32, false);
        dest.Apply(false);
        Graphics.CopyTexture(renderTex, dest);
        return dest;
    }
}