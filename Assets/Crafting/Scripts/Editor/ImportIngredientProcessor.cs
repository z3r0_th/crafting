﻿using UnityEditor;
using UnityEngine;
using System;
using Crafting;

public class ImportIngredientProcessor : AssetPostprocessor
{
    static ImportIngredientProcessor()
    {
        EditorApplication.projectWindowItemOnGUI += ProjectWindowItemOnGUI;

        /*Texture2D tex = new Texture2D(1, 1, TextureFormat.RGBA32, false) { alphaIsTransparency = true };
        tex.SetPixels(new []{Color.clear});
        tex.Apply();
        Type editorGUIUtilityType = typeof(EditorGUIUtility);
        System.Reflection.BindingFlags bindingFlags = System.Reflection.BindingFlags.InvokeMethod | System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic;
        Ingredient g = Ingredient.NewIngredient("", null);
        object[] args = { g, tex };
        editorGUIUtilityType.InvokeMember("SetIconForObject", bindingFlags, null, null, args);*/
    }

    private static void ProjectWindowItemOnGUI(string guid, Rect selectionRect)
    {
        var assetPath = AssetDatabase.GUIDToAssetPath(guid);
        var obj = AssetDatabase.LoadAssetAtPath<Ingredient>(assetPath);
        if (obj == null) return;
        if (obj.Icon == null || obj.Icon.texture == null) return;
        if (selectionRect.height > 21) return;
        selectionRect.width = selectionRect.height;
        GUI.DrawTexture(selectionRect, obj.Icon.texture);
    }
}
