﻿using Crafting;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Recipe))]
public class RecipeEditor : Editor
{
    public override void OnInspectorGUI()
    {
        // result
        EditorGUILayout.PrefixLabel(new GUIContent("Result"));
        GUILayout.BeginHorizontal();
        EditorGUILayout.ObjectField(serializedObject.FindProperty("result"), typeof(Ingredient), new GUIContent(""));
        serializedObject.FindProperty("amount").intValue = EditorGUILayout.IntSlider(serializedObject.FindProperty("amount").intValue, 0, 99);
        GUILayout.EndHorizontal();

        EditorGUILayout.Space();
        
        EditorGUILayout.PropertyField(serializedObject.FindProperty("ingredientList"));
        serializedObject.ApplyModifiedProperties();
    }
}