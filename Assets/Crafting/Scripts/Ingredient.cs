﻿using System;
using UnityEngine;

namespace Crafting
{
    [CreateAssetMenu(fileName = "ingredient", menuName = "Crafting/Ingredient", order = 0)]
    public class Ingredient : ScriptableObject, IIngredient
    {
        public static Ingredient NewIngredient(string displayName, Sprite icon)
        {
            var obj = CreateInstance<Ingredient>();
            obj.displayName = displayName;
            obj.icon = icon;

            return obj;
        }

        public bool CorrectEditorIcon { get; set; }
        [SerializeField] private string displayName = string.Empty;
        [SerializeField] private Sprite icon = null;

        public string DisplayName
        {
            get => displayName;
            set => displayName = value;
        }

        public Sprite Icon => icon;
    }
}

