﻿using System.Collections.Generic;
using UnityEngine;

namespace Crafting
{
    [CreateAssetMenu(fileName = "recipeBook", menuName = "Crafting/Book", order = 0)]
    public class RecipeBook : ScriptableObject
    {
        public static RecipeBook NewRecipeBook(Recipe[] list)
        {
            var book = CreateInstance<RecipeBook>();
            book.recipes = list;
            return book;
        }
        
        [SerializeField] private Recipe[] recipes = null;

        public IRecipe GetFirstRecipe(IRecipeIngredient[] ingredients)
        {
            for (var i = 0; i < recipes.Length; ++i)
            {
                if (recipes[i] == null) continue;
                if (recipes[i].HowManyResultsWith(ingredients) > 0) return recipes[i];
            }
            
            return null;
        }

        public IRecipe[] GetAllPossibleRecipes(IRecipeIngredient[] ingredients)
        {
            List<IRecipe> result = new List<IRecipe>();
            for (var i = 0; i < recipes.Length; ++i)
            {
                if (recipes[i] == null) continue;
                if (recipes[i].HowManyResultsWith(ingredients) > 0) result.Add(recipes[i]);
            }
            
            return result.ToArray();
        }

    }
}