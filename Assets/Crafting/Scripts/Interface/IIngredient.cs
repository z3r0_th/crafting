﻿using System.ComponentModel;

namespace Crafting
{
    public interface IIngredient
    {
        string DisplayName { get; }
    }
}