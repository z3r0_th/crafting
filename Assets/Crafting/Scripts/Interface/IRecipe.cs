﻿namespace Crafting
{
    public interface IRecipe
    {
        RecipeIngredient[] IngredientList { get; }
        Ingredient Result { get; }
        int Amount { get; }
        
        int HowManyResultsWith(IRecipeIngredient[] list, bool exactMatch=false);
    }

    public interface IRecipeIngredient
    {
        IIngredient Ingredient { get; }
        int Amount { get; }
    }
}