﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System;

namespace Crafting
{
    [CreateAssetMenu(fileName = "recipe", menuName = "Crafting/Recipe", order = 0)]
    public class Recipe : ScriptableObject, IRecipe
    {
        public static Recipe NewRecipe(RecipeIngredient[] list, Ingredient result)
        {
            var recipe = CreateInstance<Recipe>();
            recipe.ingredientList = list;
            recipe.result = result;
            
            return recipe;
        }
        
        [SerializeField] private RecipeIngredient[] ingredientList = new RecipeIngredient[]{};
        
        [SerializeField] private Ingredient result = null;
        [SerializeField] private int amount = 1;

        public RecipeIngredient[] IngredientList => ingredientList;
        
        public Ingredient Result => result;
        public int Amount => amount; 
        
        public int HowManyResultsWith(IRecipeIngredient[] pIngredients, bool exactMatch=false)
        {
            if (pIngredients == null || pIngredients.Length == 0) return 0;
            var n = int.MaxValue;
            if (exactMatch && pIngredients.Length != ingredientList.Length) return 0;
            for (var i = 0; i < IngredientList.Length; ++i)
            {
                var recipeIngredient = IngredientList[i];
                if (recipeIngredient.Ingredient == null) throw new Exception($"Some Ingredient is null in recipe");
                if (recipeIngredient.Amount <= 0) continue;
                var pIngredient = GetIngredient(pIngredients, recipeIngredient.Ingredient);
                if (pIngredient == null || pIngredient.Amount < recipeIngredient.Amount) return 0;

                var amount = Mathf.FloorToInt((float)pIngredient.Amount / recipeIngredient.Amount);
                if (amount < n) n = amount;
            }
            
            return n;
        }

        private static IRecipeIngredient GetIngredient(IEnumerable<IRecipeIngredient> list, IIngredient ingredient)
        {
            if (ingredient == null) throw new ArgumentNullException(nameof(ingredient));
            if (list == null) throw new ArgumentNullException(nameof(list));
            return list.SingleOrDefault(s => s.Ingredient == ingredient);
        }
    }

    [Serializable]
    public struct RecipeIngredient : IRecipeIngredient
    {
        [SerializeField] private Ingredient ingredient;
        [SerializeField] private int amount;

        public RecipeIngredient(Ingredient ingredient, int amount)
        {
            this.ingredient = ingredient;
            this.amount = amount;
        }

        public IIngredient Ingredient => ingredient;
        public int Amount => amount;
    }
}