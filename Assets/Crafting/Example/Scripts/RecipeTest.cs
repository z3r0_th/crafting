﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Crafting;

public class RecipeTest : MonoBehaviour
{
    public RecipeBook book;

    public List<RecipeIngredient> ingredients;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            var recipe = book.GetFirstRecipe(ingredients.Cast<IRecipeIngredient>().ToArray());
            if (recipe == null)
            {
                Debug.Log("Could not find a recipe");
                return;
            }
            
            var howMany = recipe.HowManyResultsWith(ingredients.Cast<IRecipeIngredient>().ToArray());
            Debug.Log("Transmutation Result is: " + recipe.Result + "," + howMany);
        }
    }
}
