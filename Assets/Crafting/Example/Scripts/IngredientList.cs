﻿using UnityEngine;

namespace Crafting.Example
{
    public class IngredientList : MonoBehaviour
    {
        [SerializeField] private CraftingTable target;
        [SerializeField] private Ingredient[] ingredients = null;
        [SerializeField] private RectTransform content;
        [SerializeField] private ItemSlot prefab;
        
        void Start()
        {
            foreach (Ingredient ingredient in ingredients)
            {
                var slot = Instantiate(prefab, content);
                slot.Ingredient = ingredient;
                slot.OnDrop = OnItemDrop;
            }
        }

        private void OnItemDrop(ItemSlot obj)
        {
            if (target != null)
            {
                if (!RectTransformUtility.RectangleContainsScreenPoint(target.transform as RectTransform,
                    Input.mousePosition)) return;
                target.Accept(obj);
            }
        }
    }
}
