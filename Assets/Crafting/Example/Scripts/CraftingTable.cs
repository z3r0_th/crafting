﻿using System.Collections.Generic;
using UnityEngine;

namespace Crafting.Example
{
    public class CraftingTable : MonoBehaviour
    {
        [SerializeField] private RecipeBook book;
        [SerializeField] private ItemSlot prefab = null;
        [SerializeField] private RectTransform list = null;
        [SerializeField] private ResultTable resultTable = null;
        
        private readonly List<ItemSlot> slots = new List<ItemSlot>();
        public void Accept(ItemSlot slotObj)
        {
            var slot = slots.Find(x => x.Ingredient == slotObj.Ingredient);
            if (slot == null)
            {
                slot = Instantiate(prefab, list);
                slot.Count = 1;
                slot.Ingredient = slotObj.Ingredient;
                slot.OnDrop = OnItemDrop;
                slots.Add(slot);
            } else slot.Count += 1;
            
            OnTableChange();
        }

        private void OnItemDrop(ItemSlot obj)
        {
            if (RectTransformUtility.RectangleContainsScreenPoint(transform as RectTransform,
                Input.mousePosition)) return;

            slots.Remove(obj);
            Destroy(obj.gameObject);
            OnTableChange();
        }

        private void OnTableChange()
        {
            resultTable.Clear();
            var ingredients = new List<IRecipeIngredient>();
            foreach (var slot in slots)
            {
                ingredients.Add(new RecipeIngredient(slot.Ingredient, slot.Count));
            }

            if (ingredients.Count == 0) return;
            var recipes = book.GetAllPossibleRecipes(ingredients.ToArray());
            if (recipes == null) return;
            resultTable.SetResults(recipes, ingredients.ToArray());
        }
    }
}
