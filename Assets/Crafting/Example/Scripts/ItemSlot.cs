﻿using System;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

namespace Crafting.Example
{
    public class ItemSlot : MonoBehaviour, IDragHandler, IEndDragHandler, IPointerDownHandler
    {
        public Action<ItemSlot> OnDrop;

        public int Count
        {
            get => counter;
            set
            {
                counter = value;
                if (counter == 0)
                {
                    Destroy(gameObject);
                }
                if (counterGroup == null || label == null) return;
                label.text = counter.ToString();
                counterGroup.SetActive(counter>1);
            }
        }

        public Ingredient Ingredient
        {
            get => ingredient;
            set
            {
                ingredient = value;
                UpdateItem();
            }
        }
        [SerializeField] private GameObject counterGroup = null;
        [SerializeField] private Text label = null;
        [SerializeField] private Image image = null;
        [SerializeField] private Ingredient ingredient = null;

        private int counter = 0;
        private ItemSlot objectToDrag = null;
        private Vector2 mouseDifference = Vector2.zero;

        private void Awake()
        {
            counter = 1;
            objectToDrag = null;
        }

        private void Start()
        {
            UpdateItem();
        }

        void UpdateItem()
        {
            image.sprite = ingredient.Icon;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (objectToDrag == null)
            {
                objectToDrag = Instantiate(this, GetRoot(transform as RectTransform));
                (objectToDrag.transform as RectTransform).sizeDelta = new Vector2(75,75);
            }
            
            objectToDrag.transform.position = eventData.position + mouseDifference;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            OnDrop?.Invoke(this);
            Destroy(objectToDrag.gameObject);
            objectToDrag = null;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            var positionVec3 = transform.position;
            var positionVec2 = new Vector2(positionVec3.x, positionVec3.y);
            mouseDifference = positionVec2 - eventData.position;
        }

        private RectTransform GetRoot(RectTransform rectTransform)
        {
            var parent = rectTransform.parent as RectTransform;
            while (parent != null)
            {
                if (parent.parent == null) return parent;
                parent = parent.parent as RectTransform;
            }

            return rectTransform;
        }
    }
}
