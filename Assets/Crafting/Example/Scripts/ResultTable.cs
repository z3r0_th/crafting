﻿using UnityEngine;

namespace Crafting.Example
{
    public class ResultTable:MonoBehaviour
    {
        public bool ExactRecipe
        {
            get => exactRecipe;
            set
            {
                exactRecipe = value; 
                Clear();
                SetResults(recipes, ingredients);
            }
        }
        
        [SerializeField] private RectTransform list;
        [SerializeField] private ItemSlot prefab;
        [SerializeField] private bool exactRecipe;

        private IRecipe[] recipes;
        private IRecipeIngredient[] ingredients;

        public void Clear()
        {
            foreach (Transform child in list.transform)
            {
                Destroy(child.gameObject);
            }
        }
        public void SetResults(IRecipe[] recipes, IRecipeIngredient[] ingredients)
        {
            this.recipes = recipes;
            this.ingredients = ingredients;
            
            foreach (IRecipe recipe in recipes)
            {
                SetResult(recipe.Result, recipe.Amount, recipe.HowManyResultsWith(ingredients, exactRecipe));
            }
        }

        private void SetResult(Ingredient ingredient, int amount, int total)
        {
            var slot = Instantiate(prefab, list);
            slot.Ingredient = ingredient;
            slot.Count = amount * total;
        }
    }
}